from flask import Flask, request
from flask_restful import Resource, Api
import os

app = Flask(__name__)
api = Api(app)

# Our list of tasks
tasks = [
    {
        'id': 1,
        'title': 'parcel delivery 0000',
        'complete': "False"
    },
    {
        'id': 2,
        'title': 'parcel return 0000',
        'complete': "False"
    }
]

class Tasks(Resource):
    def get(self):
        return tasks
    def post(self):
        # Generate an ID
        next_id = tasks[-1]['id'] + 1
        # Create a new task
        task = {'id': next_id, 'title': request.json['title'], 'completed': "False"}
        # Add it to our list
        tasks.append(task)
        return task

class Task(Resource):
    def get(self, task_id):
        # Lookup specific task
        get_task = [task for task in tasks if task['id'] == task_id]
        return get_task[0]
    def put(self, task_id):
        # Lookup specific task
        get_task = [task for task in tasks if task['id'] == task_id]
        # Update the complete status
        get_task[0]['complete'] = request.json['complete']
        task_index = tasks.index(get_task[0])
        tasks[task_index] = get_task[0]
        return get_task[0]
    def delete(self, task_id):
        # Lookup specific task
        get_task = [task for task in tasks if task['id'] == task_id]
        # Get its index
        task_index = tasks.index(get_task[0])
        # Delete it!
        del tasks[task_index]
        return '', 204

@app.route('/')
def hello():
    return 'Hello World!'

api.add_resource(Tasks, '/tasks')
api.add_resource(Task, '/tasks/<int:task_id>')

if __name__ == '__main__':
    app.run(debug=True, port=1234)

